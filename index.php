<?php 
require_once $_SERVER['DOCUMENT_ROOT'].'/backend/core/conf/config.sistema.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/backend/core/src/model/Class_consultas.php';
require_once $_SERVER["DOCUMENT_ROOT"].'/backend/core/vendor/funciones.php';
$modelo = new Class_consultas();
$db = db;
$banner = $modelo->consultBanner($db);
$acerca = $modelo->consultAcerca($db);
$personal = $modelo->consultPersonal($db);

?>
<!DOCTYPE html>
</body>
</html>
<html lang="es">
	<head>
	<!-- Required meta tags -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

		<!-- Bootstrap CSS -->
		<link href="https://fonts.googleapis.com/css2?family=Abel&family=Open+Sans:wght@300;400;700&display=swap" rel="stylesheet">
		<link rel="stylesheet" href="assets/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
		
		<link rel="stylesheet" href="assets/css/font-awesome.min.css">
		<link rel="stylesheet" href="assets/css/style.css">
		<title><?php echo $nombresistema ?></title>
		<style type="text/css">
		.banner{
			height: 400px;
			background: url('assets/img/banner/<?=$banner['imagen']?>') no-repeat  center;
			background-size: cover;
		}
		</style>

	</head>
	<body>
		<div class="header">
			<div class="row box-header">
				<div class="container">
					<div class="nav-menu">
						<a href="#" class="cerrar">x</a>
						<a href="#" class="activo">Inicio</a>
						<a href="#" class="">Galeria</a>
						<a href="#" class="">Nosotros</a>
						<a href="#" class="">Noticias</a>
						<a href="#" class="">Contactenos</a>
					</div>
				</div>
			</div>
		</div>
		
		<div class="banner">
			<div class="container">
				<div class="row box-banner">
					<div class="box-mensaje">
						<h3><?php echo $banner['titulo_msj'] ?> </h3>
						<p><?php echo $banner['mensaje'] ?> </p>
					</div>
					<div class="box-social">
						<a href="<?php echo $banner['face'] ?>" target="_blank"><img src="assets/img/fb.png" title="facebook" alt=""></a>
						<a href="<?php echo $banner['twitter'] ?>" target="_blank"><img src="assets/img/twitter.png" title="twitter" alt="" ></a>
						<a href="<?php echo $banner['correo'] ?>" target="_blank"><img src="assets/img/mail.png" title="mail	" alt=""></a>
						<a href="<?php echo $banner['instagram'] ?>" target="_blank"><img src="assets/img/feed.png" title="instagram" alt=""></a>
					</div>
				</div>
			</div>
		</div>
		
		<div class="main container">
			<div class="box-main">
				<div class="box-about">
					<div class="about">
						<h3 class="title">Acerca</h3>
				      <h6><?php echo $acerca['titulo_acerca']?></h6>
				      <p class="mt-md-4 mt-3 mb-0"><?php echo $acerca['acerca']?></p>
					</div>
					<div class="img-about ">
						<img src="assets/img/acerca/<?php echo $acerca['imagen']?>" alt="" class="shadow">
					</div>
				</div><!-- end acerca -->
				<div class="box-news">
					<section class="shadow p-3 mb-5 bg-white rounded">
						<p class="titulo">titulo de la noticia</p>
						<div class="noticia">
							Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolores in harum vero nisi, autem rerum nesciunt illum. Dolore molestias totam explicabo consequuntur quibusdam, porro, saepe corporis rem eum numquam ad.
						</div>
						<a href="#" class="btn-leer">Leer más</a>
					</section>

					
				</div>
				<!-- card personal -->
				<div class="box-personal">
					<div class="card mb-3 shadow bg-white rounded">
						<div class="card-image">
							<img src="assets/img/personal/" class="rounded-circle" alt="...">
						</div>
						<div class="card-body">
							<h5 class="card-title">Card title</h5>
							<p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
							<p class="card-text"><small class="text-muted">Puesto que desempe&ntilde;a</small></p>
							<div class="social-persona">
								<a href=""><i class="fa fa-twitter " aria-hidden="true"></i></a>
								<i class="fa fa-facebook " aria-hidden="true"></i>
								<i class="fa fa-instagram " aria-hidden="true"></i>
							</div>
						</div>
					</div>

					<div class="card mb-3 shadow bg-white rounded">
						<div class="card-image">
							<img src="assets/img/personal/" class="rounded-circle" alt="...">
						</div>
						<div class="card-body">
							<h5 class="card-title">Card title</h5>
							<p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
							<p class="card-text"><small class="text-muted">Puesto que desempe&ntilde;a</small></p>
							<div class="social-persona">
								<i class="icon-twitter"></i>
								<i class="icon-facebook-square"></i>
								<i class="icon-facebook-square"></i>
							</div>
						</div>
					</div>
<?php foreach ($personal as $key):?>
					<div class="card mb-3 shadow bg-white rounded">
						<div class="card-image">
							<img src="assets/img/personal/<?php echo $key['imagen']?>" class="rounded-circle" alt="...">
						</div>
						<div class="card-body">
							<h5 class="card-title"><?php echo $key['nombre'].' '.$key['apellido']?></h5>
							<p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
							<p class="card-text"><small class="text-muted">Puesto que desempe&ntilde;a <?php echo $key['puesto']?></small></p>
							<div class="social-persona">
								<i class="icon-twitter"></i>
								<i class="icon-facebook-square"></i>
								<i class="icon-facebook-square"></i>
							</div>
						</div>
					</div>
<?php endforeach;?>
				</div>
			</div>
			<hr>

			<!-- aside -->
			<div class="box-aside">
				<h3 class="title">Eventos pasados</h3>
				<a href="#" class="">
					<i class="fa fa-angle-double-right fa-lg" aria-hidden="true"></i>
					<span>Nombre del evento 22/02/2010</span>
				</a>
				<a href="#" class="">
					<i class="fa fa-angle-double-right fa-lg" aria-hidden="true"></i>
					<span>Nombre del evento 24/05/2010</span>
				</a>
				<a href="#" class="">
					<i class="fa fa-angle-double-right fa-lg" aria-hidden="true"></i>
					<span>Nombre del evento 10/02/2013</span>
				</a>
				<a href="#" class="">
					<i class="fa fa-angle-double-right fa-lg" aria-hidden="true"></i>
					<span>Nombre del evento 30/09/2019</span>
				</a>
				<a href="#" class="">
					<i class="fa fa-angle-double-right fa-lg" aria-hidden="true"></i>
					<span>Nombre del evento 22/02/2010</span>
				</a>
			</div>
		</div>

		<!-- site footer -->
		<footer class="site-footer">
			<!-- 
			<div class="top-footer">
				<div class="container my-md-5 my-4">
					<div class="row">
						<div class="col-lg-4">
							<div class="footer-logo mb-3">
								<a href="index.html"><span class="fa fa-shield"></span> Captivate</a>
							</div>
							<div>
								<p class="">
									Lorem ipsum dolor, sit amet consectet et adipis icing elit. Ab commodi iure minus
									laboriosam
									placeat quia, dolorem animi. Eveniet repudiandae, perferendis nesciunt deserunt iure et, consequatur optio!
								</p>
							</div>
						</div>
						
						<div class="col-lg-3 col-md-4 mt-lg-0 mt-5">
							<h4 class="footer-title">Quick Links</h4>
							<ul class="footer-list">
								<li><a href="about.html"> About company</a></li>
								<li><a href="services.html"> Explore services</a></li>
								<li><a href="#work"> How does we Work?</a></li>
								<li><a href="#projects"> View projects</a></li>
							</ul>
						</div>
						
						<div class="col-lg-5 col-md-8 mt-lg-0 mt-5">
							<h4 class="footer-title">Newsletter</h4>
							<p class="mb-4">
								By subscribing to our mailing list you will always be updated with the latest news from us.
							</p>
							
						</div>
					</div>
				</div>
			</div>
			-->
			<div class="bottom-footer">
				<div class="container">
					<div class="row">
						<div class="col-lg-8 text-lg-left text-center mb-lg-0 mb-3">
							<p class="copyright">© 2020  All Rights Reserved. Design by <a
						href="https://w3layouts.com/">William Infante</a>
						<a href="backend.php" target="_blank">Administrar</a> </p>
						</div>
						<div class="col-lg-4 align-center text-lg-right text-center">
							<a href="#"><img src="assets/img/fb.png" title="facebook" alt=""></a>
							<a href="#"><img src="assets/img/twitter.png" title="twitter" alt="" ></a>
							<a href="#"><img src="assets/img/mail.png" title="mail	" alt=""></a>
							<a href="#"><img src="assets/img/feed.png" title="instagram" alt=""></a>
						</div>
					</div>
				</div>
			</div>
		</footer>
		<div class="box-flotant">
			<a href="#">+</a>
		</div>
		<!-- Optional JavaScript -->
		<!-- jQuery first, then Popper.js, then Bootstrap JS -->
		<script src="assets/js/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
		<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
		<script src="assets/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
	</body>
</html>