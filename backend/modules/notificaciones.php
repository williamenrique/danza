<div class="profile_details_left">
	<ul class="nofitications-dropdown">
	  <li class="dropdown">
		<a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i
			class="fa fa-bell-o"></i><span class="badge blue">3</span></a>
		<ul class="dropdown-menu">
		  <li>
			<div class="notification_header">
			  <h3>You have 3 new notifications</h3>
			</div>
		  </li>
		  <li><a href="#" class="grid">
			  <div class="user_img"><img src="<?php echo img?>avatar.png" alt=""></div>
			  <div class="notification_desc">
				<p>Johnson purchased template</p>
				<span>Just Now</span>
			  </div>
			</a></li>
		  <li class="odd"><a href="#" class="grid">
			  <div class="user_img"><img src="" alt=""></div>
			  <div class="notification_desc">
				<p>New customer registered </p>
				<span>1 hour ago</span>
			  </div>
			</a></li>
		  <li><a href="#" class="grid">
			  <div class="user_img"><img src="" alt=""></div>
			  <div class="notification_desc">
				<p>Lorem ipsum dolor sit amet </p>
				<span>2 hours ago</span>
			  </div>
			</a></li>
		  <li>
			<div class="notification_bottom">
			  <a href="#all" class="bg-primary">See all notifications</a>
			</div>
		  </li>
		</ul>
	  </li>
	  <li class="dropdown">
		<a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i
			class="fa fa-comment-o"></i><span class="badge blue">4</span></a>
		<ul class="dropdown-menu">
		  <li>
			<div class="notification_header">
			  <h3>You have 4 new messages</h3>
			</div>
		  </li>
		  <li><a href="#" class="grid">
			  <div class="user_img"><img src="" alt=""></div>
			  <div class="notification_desc">
				<p>Johnson purchased template</p>
				<span>Just Now</span>
			  </div>
			</a></li>
		  <li class="odd"><a href="#" class="grid">
			  <div class="user_img"><img src="" alt=""></div>
			  <div class="notification_desc">
				<p>New customer registered </p>
				<span>1 hour ago</span>
			  </div>
			</a></li>
		  <li><a href="#" class="grid">
			  <div class="user_img"><img src="" alt=""></div>
			  <div class="notification_desc">
				<p>Lorem ipsum dolor sit amet </p>
				<span>2 hours ago</span>
			  </div>
			</a></li>
		  <li><a href="#" class="grid">
			  <div class="user_img"><img src="" alt=""></div>
			  <div class="notification_desc">
				<p>Johnson purchased template</p>
				<span>Just Now</span>
			  </div>
			</a></li>
		  <li>
			<div class="notification_bottom">
			  <a href="#all" class="bg-primary">See all messages</a>
			</div>
		  </li>
		</ul>
	  </li>
	</ul>
</div>