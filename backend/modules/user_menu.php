<div class="profile_details">
	<ul>
		<li class="dropdown profile_details_drop">
			<a href="#" class="dropdown-toggle" data-toggle="dropdown" id="dropdownMenu3" aria-haspopup="true"
			aria-expanded="false">
				<div class="profile_img">
					<img src="<?php echo img?>avatar.png " class="rounded-circle" alt="" />
					<div class="user-active">
					<span></span>
					</div>
				</div>
			</a>
			<ul class="dropdown-menu drp-mnu" aria-labelledby="dropdownMenu3">
				<li class="user-info">
					<h5 class="user-name"><?php echo $_SESSION['nombres']. " ".$_SESSION['apellidos'] ?></h5>
					<span class="status ml-2"><?php echo $_SESSION['user'] ?></span>
				</li>
				<li> <a href="usuario/"><i class="lnr lnr-user"></i>Mi Perfil</a> </li>
				<li> <a href="#"><i class="lnr lnr-users"></i>1k Followers</a> </li>
				<li> <a href="#"><i class="lnr lnr-cog"></i>Setting</a> </li>
				<li> <a href="#"><i class="lnr lnr-heart"></i>100 Likes</a> </li>
				<li class="logout"> <a href="#" onclick="salir()"><i class="fa fa-power-off"></i> Logout</a> </li>
			</ul>
		</li>
	</ul>
</div>