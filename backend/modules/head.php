<?php 
session_start();
if(!empty($_SESSION['ingreso'])):
include $_SERVER['DOCUMENT_ROOT'].'/backend/core/conf/config.sistema.php'; 
?>
<!doctype html>
<html lang="es">
<head>
	<!-- Required meta tags -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<!-- Template CSS -->
	<link rel="stylesheet" href="<?php echo CSS?>style-starter.css">
	<link rel="stylesheet" href="<?php echo CSS?>toastr.min.css">
	<link rel="stylesheet" href="<?php echo CSS?>jquery-confirm.css">
	<link rel="stylesheet" href="<?php echo css?>styles.css">
	<!-- google fonts -->
	<!-- <link href="//fonts.googleapis.com/css?family=Nunito:300,400,600,700,800,900&display=swap" rel="stylesheet"> -->
</head>
<body class="sidebar-menu-collapsed">
	<?php 
else: 
header('Location: ../');
endif;