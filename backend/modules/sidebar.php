<div class="sidebar-menu sticky-sidebar-menu">
	<!-- logo start -->
	<div class="logo">
		<h1><a href="../">Danza ST</a></h1>
	</div>
	<!-- //image logo -->
	<div class="logo-icon text-center">
		<a href="../" title="logo"><img src="<?php echo img?>logo.png" alt="logo-icon"> </a>
	</div>
	<!-- //logo end -->
	<div class="sidebar-menu-inner">
		<!-- sidebar nav start -->
		<ul class="nav nav-pills nav-stacked custom-nav">
			<li class="active"><a href="?v=home"><i class="fa fa-tachometer"></i><span> Panel Principal</span></a>
			</li>
			<li class="menu-list">
				<a href="#"><i class="fa fa-cogs"></i>
					<span>Modulos <i class="lnr lnr-chevron-right"></i></span></a>
				<ul class="sub-menu-list">
					<li><a href="?v=banner">Banner</a> </li>
					<li><a href="?v=personal">Personal</a> </li>
					<li><a href="?v=noticias">Noticias</a></li>
				</ul>
			</li>
			<li><a href="?v=personalAdm"><i class="fa fa-table"></i> <span>Personal administrativo</span></a></li>
			<li><a href="blocks.html"><i class="fa fa-th"></i> <span>Content blocks</span></a></li>
			<li><a href="forms.html"><i class="fa fa-file-text"></i> <span>Forms</span></a></li>
		</ul>
		<!-- //sidebar nav end -->
		<!-- toggle button start -->
		<a class="toggle-btn">
			<i class="fa fa-angle-double-left menu-collapsed__left"><span>Collapse Sidebar</span></i>
			<i class="fa fa-angle-double-right menu-collapsed__right"></i>
		</a>
		<!-- //toggle button end -->
	</div>
</div>