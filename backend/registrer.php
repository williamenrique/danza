 <!DOCTYPE html>
<html lang="en"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="">
	<meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
	<meta name="generator" content="Jekyll v3.8.6">
	<title>Formaulario de registro</title>
<!-- Bootstrap core CSS -->
	<link href="core/vendor/css/bootstrap.min.css" rel="stylesheet" >
	<!-- Favicons -->
	<link rel="apple-touch-icon" href="https://getbootstrap.com/docs/4.4/assets/img/favicons/e-touch-icon.png" sizes="180x180">
	<style>
		.bd-placeholder-img {
			font-size: 1.125rem;
			text-anchor: middle;
			-webkit-user-select: none;
			-moz-user-select: none;
			-ms-user-select: none;
			user-select: none;
		}
		

	@media (min-width: 768px) {
		.bd-placeholder-img-lg {
			font-size: 3.5rem;
		}
	}
	</style>
	<!-- Custom styles for this template -->
	<link href="core/vendor/css/font-awesome.min.css" rel="stylesheet" type="text/css" media="all">
	<link rel="stylesheet" href="assets/css/styles-login.css">
	<link rel="stylesheet" href="assets/css/floating-labels.css">
	<link href="core/vendor/css/jquery-confirm.css" rel="stylesheet">
	<link href="core/vendor/css/toastr.min.css" rel="stylesheet">
	<link href="assets/css/styles.css" rel="stylesheet">
</head>
<body>
	<?php include 'forms/registrer.php'?>
	<script src="core/vendor/js/jquery-3.3.1.min.js"></script>
	<script src="core/vendor/js/jquery-1.10.2.min.js"></script>
	<script src="core/vendor/js/bootstrap.min.js"></script>
	<script src="core/vendor/js/toastr.min.js"></script>
	<script src="core/vendor/js/jquery-confirm.js"></script>
	<script src="assets/js/login.js"></script>
</body>
</html>