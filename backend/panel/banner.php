<?php 
if ($_GET['v'] != "banner"):
 	header("Location: ?v=home");
else: 
?>
<title><?php echo $nombresistema?>-Banner</title>
<div class="se-pre-con"></div>


<section>
	<div class="form banner">
		<div class="box-form-banner">
			<p>Introduzca titulo y el mensaje del banner </p>
			<form id="formMsn">
				<div class="form-row">
					<div class="box-input-mensaje form-group col-md-12 col-lg-3">
						<input type="text" spellcheck="false" class="form-control"  id="boxtitulomsj" name="boxtitulomsj" value="<?php echo $banner['titulo_msj'] ?>">
					</div>
					<div class="box-input-mensaje form-group col-md-12 col-lg-9">
						<input type="text" spellcheck="false" class="form-control"  id="boxmsj" name="boxmsj" value="<?php echo $banner['mensaje'] ?>">
					</div>
				</div>
				<div class="box-social mt-4">
					<p>Cambio de Redes sociales</p>
					<div class="form-row align-items-center">
						<div class="col-sm-6 col-lg-3 my-1">
							<div class="input-group">
								<div class="input-group-prepend">
									<img src="../assets/img/fb.png" alt="">
								</div>
								<input type="text" spellcheck="false" class="form-control" id="face" name="face" placeholder="Facebook" value="<?php echo $banner['face'] ?>">
							</div>
						</div>
						<div class="col-sm-6 col-lg-3 my-1">
							<div class="input-group">
								<div class="input-group-prepend">
									<img src="../assets/img/twitter.png" alt="">
								</div>
								<input type="text" spellcheck="false" class="form-control" id="twitter" name="twitter" placeholder="Twitter" value="<?php echo $banner['twitter'] ?>">
							</div>
						</div>
						<div class="col-sm-6 col-lg-3 my-1">
							<div class="input-group">
								<div class="input-group-prepend">
									<img src="../assets/img/feed.png" alt="">
								</div>
								<input type="text" spellcheck="false" class="form-control" id="instagram" name="instagram" placeholder="Instagram" value="<?php echo $banner['instagram'] ?>">
							</div>
						</div>
						<div class="col-sm-6 col-lg-3 my-1">
							<div class="input-group">
								<div class="input-group-prepend">
									<img src="../assets/img/mail.png" alt="">
								</div>
								<input type="text" spellcheck="false" class="form-control" id="mail" name="mail" placeholder="Correo" value="<?php echo $banner['correo'] ?>">
							</div>
						</div>
						
						<button type="button" class=" btn btn-primary mt-2" onclick="actualizarMsn()"> Actualizar</button>
					</div>
				</div>
			</form>
		</div> <!-- end box-form-banner- -->
		
		<form class="box-image" name="form_file" id="form_file" enctype="multipart/form-data" method="POST">
			<div class="box-form-image" id="add_photo">
				<i class="fa fa-camera icon-buscar"></i>
				<p>Imagen para el banner   </p>
			</div>
			<input id="file-input" accept="image/png, image/jpeg" name="uploadedfile1" type="file"/>
			<input id="tipo" name="tipo" type="hidden" value="banner">
		</form>
		<div class="box-previa" id="box-previa">
		<?php if($banner['imagen'] == ""): ?>
			<div class="preview"></div>
		<?php else: ?>
			<section id="Images" class="images-cards">
				<img src="../assets/img/banner/<?php echo $banner['imagen']?>" alt="">
			</section>
		<?php endif; ?>
		</div>
		<button type="button" class="btn btn-danger btn-sm mt-2" onclick="subir_archivo()">Guardar imagen</button>
	</div><!-- end box-form- -->

	<div class="box-for-acerca">
		<div class="box-cont-acerca">
			<div class="titulo">Introduzca una reseña o historia</div>
			<form class="form_acerca mt-2" id="form_acerca">
				<div class="form-group">
					<label for="titulo_acerca">Titulo acerca</label>
					<input type="text" spellcheck="false" id="titulo_acerca" name="titulo_acerca" class="form-control" value="<?php echo $acerca['titulo_acerca'] ?> ">
				</div>
				<div class="form-group">
					<textarea class="form-control " name="box_acerca" id="box_acerca"  style="height: 160px;" spellcheck="false">
				<?php echo $acerca['acerca']?>
					</textarea>
				</div>
			</form>
			<button type="button" class="btn btn-danger btn-acerca btn-sm" data-toggle="tooltip" data-placement="bottom" title="Tooltip on bottom" onclick="actualizarAcerca()">Actualizar</button>
		</div>
		<div class="box-cont-img-acerca">
			<div class="box-buscar">
				<div class="open_search">
					<div id="open_search">
						<i class="fa fa-camera icon-buscar-img"></i>
						<p>Buscar imagen</p>
					</div>
					<button type="button" class="btn btn-secondary btn-sm btn-acerca-img" onclick="guardar_imagen_acerca()">Guardar imagen</button>
				</div>
				<div class="section box-img-acerca" id="box-img-acerca">
				<?php if($acerca != ""):?>
					<section id="imagen" class="images-card">
						<div class="container-imagen">
							<img src="../assets/img/acerca/<?php echo $acerca['imagen']?>" alt="">
						</div>
					</section>
				<?php else: ?>
					<div class="box-previa-about"></div>
				<?php endif; ?>
				</div>
				<form class="form_img_acerca mt2" name="form_img_acerca" id="form_img_acerca" enctype="multipart/form-data" method="POST">

					<input id="file-input-acerca" accept="image/png, image/jpeg" name="uploadedfile1" type="file"/>
					<input id="tipo" name="tipo" type="hidden" value="acerca">
				</form>
			</div>
		</div>
	</div>
</section>

		<!-- end main content -->
<?php 
endif;