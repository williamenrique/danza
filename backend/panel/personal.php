<?php 
if ($_GET['v'] != "personal"):
 	header("Location: ?v=home");
else: 

?>
<title><?php echo $nombresistema?>-Personal</title>
<div class="se-pre-con"></div>

<section class="container">
	<form method="post" enctype="multipart/form-data" id="form_img_persona" name="form_img_persona">
		<div class="cabecera-form">
			<div class="box-titulo">
				<h2 class="titulo">Formulario de registro de personal</h2>
				<h3 class="mt-2">Ingrese sus datos</h3>
			</div>
			<div class="box-img-personal" id="box-img-personal">
				<div class="box-default">
					<i class="fa fa-camera" id="imagen-persona"></i>
				</div>
			</div>
				<input id="file-input-persona" accept="image/png, image/jpeg" name="uploadedfile1" type="file"/>
				<input id="tipo" name="tipo" type="hidden" value="personal">
		</div>
	</form>
	<form  class="form-personal" id="form-personal">

		<div class="form-row">
			<div class="form-group col-sm-6 col-md-4 col-lg-4 col-xl-4">
				<label for="nombrePersonal">Nombre</label>
				<input type="text" class="form-control" id="nombrePersonal" name="nombrePersonal">
			</div>
			<div class="form-group col-sm-6 col-md-4 col-lg-4 col-xl-4">
				<label for="apellidoPersonal">Apellido</label>
				<input type="text" class="form-control" id="apellidoPersonal" name="apellidoPersonal">
			</div>
			<div class="form-group col-sm-2 col-md-1 col-lg-1 col-xl-1">
				<label for="edadPersonal">Edad</label>
				<input type="text" class="form-control" id="edadPersonal" name="edadPersonal">
			</div>
			<div class="form-group col-sm-4 col-md-3 col-lg-3 col-xl-3">
				<label for="cedulaPersonal">Cedula</label>
				<input type="text" class="form-control" id="cedulaPersonal" name="cedulaPersonal" >
			</div>
		</div>
		<div class="form-row">
			<div class="form-group col-md-8">
				<label for="direccionPersonal">Dirección</label>
				<input type="text" class="form-control" id="direccionPersonal" name="direccionPersonal">
			</div>
			<div class="form-group col-xs-12 col-md-4 pt-4">
				<div class="custom-control custom-radio custom-control-inline ">
				<input type="radio" id="generoPersonal" name="generoPersonal" class="custom-control-input" value="m">
				<label class="custom-control-label" for="generoPersonal">Masculino</label>
				</div>
				<div class="custom-control custom-radio custom-control-inline">
				<input type="radio" id="generoPersonal1" name="generoPersonal" class="custom-control-input" value="f">
				<label class="custom-control-label" for="generoPersonal1">Femenino</label>
				</div>
			</div>
		</div>
		<div class="form-row">
			<div class="form-group col-xs-12 col-md-4 estado">
				<label for="inputState">Estado</label>
				<select  class="form-control" name="id_estado" id="estado">
					<option selected>Seleccione</option>
			<?php foreach ($estado as $key):?>
					<option value="<?php echo $key['id_estado'] ?>"><?php echo $key['estado'] ?></option>
			<?php endforeach;?>
				</select>
			</div>
			<div class="form-group col-xs-12 col-md-4 ciudad">
				<label for="ciudad">Ciudad</label>
				<select class="custom-select" id="ciudad" name="id_ciudad">
					<option>Seleccione</option>
				</select>
			</div>
			<div class="form-group col-xs-12 col-md-4 municipio">
				<label for="municipio">Municipio</label>
				<input type="text" class="form-control" id="municipio" name="municipio" readonly>
				<input type="hidden" class="form-control" id="id_municipio" name="id_municipio" readonly>
			</div>
		</div>
		<div class="form-row">
			<div class="form-group col-xs-6 col-md-4">
				<label for="tlfPersonal">Telefono</label>
				<input type="text" class="form-control" id="tlfPersonal" name="tlfPersonal" placeholder="Opcional">
			</div>
			<div class="form-group col-xs-6 col-md-4">
				<label for="correoPersonal">Correo</label>
				<input type="text" class="form-control" id="correoPersonal" name="correoPersonal" placeholder="Opcional">
			</div>
			<div class="form-group col-xs-6 col-md-4">
				<label for="puestoPersonal">Puesto</label>
				<select class="custom-select" name="puestoPersonal" id="puestoPersonal">
					<option>Seleccione</option>
			<?php foreach ($puesto as $IDpuesto):?>
				<option value="<?php echo $IDpuesto['id_puesto']?>"><?php echo $IDpuesto['puesto']?></option>
			<?php endforeach; ?>
				</select>
			</div>
		</div>
		<hr>
		<h3 class="titulo">Datos del representante</h3>
		<h5 class="mb-2">Representante #1</h5>
		<div class="form-row">
			<div class="form-group col-sm-6 col-md-4 col-lg-4 col-xl-3">
				<label for="representante1">Nombre y Apellido</label>
				<input type="text" class="form-control" id="representante1" name="representante1" placeholder="">
			</div>
			<div class="form-group col-sm-6 col-md-4 col-lg-2 col-xl-3">
				<label for="cedulaRep1">Cedula</label>
				<input type="text" class="form-control" id="cedulaRep1" name="cedulaRep1" placeholder="">
			</div>
			<div class="form-group col-sm-6 col-md-4 col-lg-2 col-xl-3">
				<label for="tlfrep1">Telefono</label>
				<input type="text" class="form-control" id="tlfrep1" name="tlfrep1" placeholder="">
			</div>
			<div class="form-group col-sm-6 col-md-6 col-lg-4 col-xl-3">
				<label for="correoRep1">Correo</label>
				<input type="text" class="form-control" id="correoRep1" name="correoRep1" placeholder="">
			</div>
		</div>
		<h5 class="mb-2">Representante #2</h5>
		<div class="form-row">
			<div class="form-group col-sm-6 col-md-4 col-lg-4 col-xl-3">
				<label for="representante2">Nombre y Apellido</label>
				<input type="text" class="form-control" id="representante2" name="representante2" placeholder="Opcional">
			</div>
			<div class="form-group col-sm-6 col-md-4 col-lg-2 col-xl-3">
				<label for="cedulaRep2">Cedula</label>
				<input type="text" class="form-control" id="cedulaRep2" name="cedulaRep2" placeholder="Opcional">
			</div>
			<div class="form-group col-sm-6 col-md-4 col-lg-2 col-xl-3">
				<label for="tlfrep2">Telefono</label>
				<input type="text" class="form-control" id="tlfrep2" name="tlfrep2" placeholder="Opcional">
			</div>
			<div class="form-group col-sm-6 col-md-6 col-lg-4 col-xl-3">
				<label for="correoRep2">Correo</label>
				<input type="text" class="form-control" id="correoRep2" name="correoRep2" placeholder="Opcional">
			</div>
		</div>
	</form>
		<button type="button" class="btn btn-secondary mt-2" onclick="guardarPersona()">Agregar</button>
	<hr>

</section>
<?php 
endif;
