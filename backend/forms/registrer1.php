
<div class="form-signin">
	<div class="text-center mb-4">
		<img class="mb-4" src="./Floating labels example · Bootstrap_files/bootstrap-solid.svg" alt="" width="72" height="72">
		<h1 class="h3 mb-3 font-weight-normal">Formaulario de Registro</h1>
		<p>Ingrese sus datos para el registro <code>:placeholder-shown</code> pseudo-element. <a href="https://caniuse.com/#feat=css-placeholder-shown">Works in latest Chrome, Safari, and Firefox.</a></p>
	</div>
	<div class="box container">
		<div class="box-registrer">
			<div class="box-form">
				<div class="form-header">
					<h4>Registrese</h4>
				</div>
				<form id="form_crear">
					<div class="form-row">
						<div class=" col-md-6 ">
							<div class="form-label-group">
								<input type="text" id="nombre" name="nombre" class="form-control" placeholder="Ingrese su nombre" required="" autofocus="">
								<label for="nombre">Nombre</label>
							</div>
						</div>
						<div class=" col-md-6 ">
							<div class="form-label-group">
								<input type="text" id="apellido" name="apellido" class="form-control" placeholder="Ingrese su apellido" required="" autofocus="">
								<label for="apellido">Apellido</label>
							</div>
						</div>
					</div>
					<div class="form-label-group">
						<input type="email" id="email" name="email" class="form-control" placeholder="Direccion de correo" required="" autofocus="">
						<label for="email">Direccion de correo</label>
					</div>
					<div class="form-row">
						<div class=" col-md-6 ">
							<div class="form-label-group">
								<input type="text" id="user" name="user" class="form-control" placeholder="Cree su usuario" required="" autofocus="">
								<label for="nombre">Usuario</label>
							</div>
						</div>
						<div class=" col-md-6 ">
							<div class="form-label-group">
								<input type="password" id="pass" name="pass" class="form-control" placeholder="Nueva clave" required="" autofocus="">
								<label for="pass">Nueva clave</label>
							</div>
						</div>
					</div>
					<button type="submit" class="btn btn-primary btn-enviar" onclick="insertUsuario()">Crear usuario</button>
					<a href="../" class="btn btn-link">Iniciar sesion</a>
				</form>
			</div>
		</div>
	</div>
</div>