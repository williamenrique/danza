<style type="text/css" media="screen">
	.form-label-group > label {
		padding-left: 2rem !important;
		padding-top: .80rem !important;
	}
</style>
<div class="signupform">
	<div class="container">
		<!-- main content -->
		<div class="agile_info">
			<div class="w3l_form">
				<div class="left_grid_info">
					<h1>Cree su cuenta personal</h1>
					<p>Cree maneje y administre sus cuentas email desde una misma aplicacion, tenga a la mano sus datos personales y poder llevar todo a tiempo.</p>
					<img src="app/assets/images/b1.png" alt="" />
				</div>
			</div>
			<div class="w3_info">
				<h2>Ingrese a su cuenta</h2>
				<p>Ingrese sus datos para iniciar sesión.</p>
				<form id="form_crear">
					<div class="form-row">
						<div class=" col-md-6 ">
							<div class="input-group form-label-group">
								<span class="fa fa-envelope" aria-hidden="true"></span>
								<input type="text" id="nombre" name="nombre" class="" placeholder="Ingrese su nombre" required="" autofocus="">
								<label for="nombre">Nombre</label>
							</div>
						</div>
						<div class=" col-md-6 ">
							<div class="input-group form-label-group">
								<span class="fa fa-envelope" aria-hidden="true"></span>
								<input type="text" id="apellido" name="apellido" class="" placeholder="Ingrese su apellido" required="" autofocus="">
								<label for="apellido">Apellido</label>
							</div>
						</div>
					</div>
					<div class="input-group form-label-group">
						<span class="fa fa-envelope" aria-hidden="true"></span>
						<input type="email" id="email" name="email" class="" placeholder="Direccion de correo" required="" autofocus="">
						<label for="email">Direccion de correo</label>
					</div>
					<div class="form-row">
						<div class=" col-md-6 ">
							<div class="input-group form-label-group">
								<span class="fa fa-envelope" aria-hidden="true"></span>
								<input type="text" id="user" name="user" class="" placeholder="Cree su usuario" required="" autofocus="">
								<label for="nombre">Usuario</label>
							</div>
						</div>
						<div class=" col-md-6 ">
							<div class="input-group form-label-group">
								<span class="fa fa-envelope" aria-hidden="true"></span>
								<input type="password" id="pass" name="pass" class="" placeholder="Nueva clave" required="" autofocus="">
								<label for="pass">Nueva clave</label>
							</div>
						</div>
					</div>
					<button type="submit" class="btn btn-danger btn-enviar" onclick="insertUsuario()">Crear usuario</button>
					<a href="../" class="btn btn-light">Iniciar sesion</a>
				</form>
			</div>
		</div>
		<!-- //main content -->
	</div>
	<!-- footer -->
	<div class="footer">
		<p>&copy; 2020 Sistema de agenda. Todos los Derechos Reservados | Desarrollado por <a href="https://w3layouts.com/" target="blank">William Enrique Infante</a></p>
	</div>
	<!-- footer -->
</div>