<div class="signupform">
	<div class="container">
		<!-- main content -->
		<div class="agile_info">
			<div class="w3l_form">
				<div class="left_grid_info">
					<h1>Administre su sitio personal</h1>
					<p>Maneje y administre su sitio email, cambiando apariencia y contenido al momento desde una misma aplicacion, tenga a la mano sus datos personales y poder llevar todo a tiempo.</p>
					<img src="backend/assets/images/image.jpg" alt="" />
				</div>
			</div>
			<div class="w3_info">
				<h2>Ingrese a su cuenta</h2>
				<p>Ingrese sus datos para iniciar sesión.</p>
				<form id="form_login">
					<div class="input-group form-label-group">
						<span class="fa fa-envelope" aria-hidden="true"></span>
						<input type="text" placeholder="Enter Your Email" required="" id="email" name="email"> 
						<label class="ml-4 pl-4" for="email">Email o Usuario</label>
					</div>
					<div class="input-group form-label-group">
						<span class="fa fa-lock" aria-hidden="true"></span>
						<input type="Password" placeholder="Enter Password" required="" id="clave" name="clave">
						<label class="ml-4 pl-4">Password</label>
					</div> 				
						<button class="btn btn-danger btn-block" type="submit" name="btn-login"  onclick="login()">Login</button >                
				</form>
				<p class="account">Al hacer clic en iniciar sesión, aceptas nuestro <a href="#">Terms & Condiciones!</a></p>
				<p class="account1">Todavia no tienes una cuenta? <a href="backend/registrer.php">Registrese aqui</a></p>
			</div>
		</div>
		<!-- //main content -->
	</div>
	<!-- footer -->
	<div class="footer">
		<p>&copy; 2020 Sistema de personalización. Todos los Derechos Reservados | Desarrollado por <a href="https://w3layouts.com/" target="blank">William Enrique Infante</a></p>
	</div>
	<!-- footer -->
</div>