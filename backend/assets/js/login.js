function login(){
	$('#form_login').submit(function (e) {
		$(".btn-login").attr('disabled', 'true');
		$(".espera").show();
		dataString = $("#form_login").serialize();
		$.ajax({
			type: "POST", 
			url: "backend/core/src/controller/tbl_login.php", 
			data: dataString+"&accion=login",
			dataType: "json",
			success: function ( data, statusCode, xhr ) {
				if (data.estado=="autenticado"){
					location.href="backend/?v=home";
					//location.href="backend/";
				}else if(data.estado=="inactivo"){
					toastr["info"]("usted esta inactivo")
				}else{
					$('#barra').hide();
					$("#btn-login").removeAttr('disabled');
					toastr["error"]("Usuario no existe o clave errada")
					$("#form_login")[0].reset();
					$('#email').focus();
					$(".btn-login").removeAttr('disabled');
					$(".espera").hide();
				}
			}
		})
	 	e.preventDefault();
        return false;
    })
}


function insertUsuario(){

	
	if($("#nombre").val() == ""){
		$.alert("Debe colocar un nombre")
		$("#nombre").focus();
		return false;
	}
	if($("#apellido").val() == ""){
		$.alert("Debe colocar un apellido")
		$("#apellido").focus();
		return false;
	}
	if($("#email").val() == ""){
		$.alert("Debe colocar un correo electronico")
		$("#email").focus();
		return false;
	}
	if($("#user").val() == ""){
		$.alert("Debe ingresar un usuario")
		$("#user").focus();
		return false;
	}
	
	if($("#pass").val() == ""){
		$.alert("Debe ingresar una clave")
		$("#pass").focus();
		return false;
	}
	return true;
	
}


$(".btn-enviar").on('click',function(){
	if(insertUsuario()){
		dataString = $("#form_crear").serialize();
		$.ajax({
			type: "POST",
			url: "/backend/core/src/controller/tbl_login.php", 
			data: dataString+"&accion=insertar",
			dataType: "json",
			success: function ( data, statusCode, xhr ) {
				if(data.estado == "existe"){
					$.alert("Usuario o correo esta en uso")
				}else{
					if(data.estado == "completo"){
						$("#form_crear")[0].reset();
						$('#nombre').focus();
						$(".btn-enviar").removeAttr('disabled');
						$(".envio").show();
						$(".espera").hide();
						toastr.success("Usuario Agregado");
						location.href="../backend.php";
					}else if(data.estado == "error"){
						toastr["error"]("Ocurrio un error")
						$(".btn-enviar").removeAttr('disabled');
						$(".envio").show();
						$(".espera").hide();
					}
				}
			}
		})
	}
})
