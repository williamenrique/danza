function salir(){
	$.ajax({
		type: "POST", //The type of HTTP verb (post, get, etc)
		url: "../../backend/core/src/controller/tbl_usuario.php", //The URL from the form element where the AJAX request will be sent
		data: { "accion": "salir" }, //All the data from the form serialized
		dataType: "json", //The type of response to expect from the server
		success: function ( data, statusCode, xhr ) { //Triggered after a successful response from server
			if (data.estado=="hecho"){
				location.href="../";
			}
		}
	});
}

function actualizarMsn(){
	dataString = $("#formMsn").serialize();
	$.ajax({
		url: '../backend/core/src/controller/tbl_consultas.php',
		type: 'POST',
		dataType: 'json',
		data: dataString+"&accion=actualizarMsn",
		success: function(data, statusCode, xhr){
			if(data.estado == "actualizado"){
				toastr["success"]("Actualizado")
			}else{
				toastr["error"]("A ocurrido un erro")
			}
		}
	})
}

$(document).on("click", "#add_photo", function(){
	$("#file-input").click();
})

$(document).on("change", "#file-input", function(){
	//console.log(this.files);
	var files = this.files;
	var element;
	var supportedImages = ["image/jpeg", "image/png", "image/gif"];
	var seEncontraronElementoNoValidos = false;
	for (var i = 0; i < files.length; i++) {
		element = files[i];
		if (supportedImages.indexOf(element.type) != -1) {
			createPreview(element);
		}
		else {
			seEncontraronElementoNoValidos = true;
		}
	}
})

function createPreview(file) {
	var imgCodified = URL.createObjectURL(file);
	$(".preview").css('display', 'none');
	document.getElementById("box-previa").innerHTML='<section id="Images" class="images-cards"><img src="'+imgCodified+'" alt=""><figure><figcaption> <i class="fa fa-close"></i></figcaption></figure></section>';
}
$(document).on("click", "#Images", function(e){
	document.getElementById("box-previa").innerHTML='<div class="preview"></div>';
	$(this).parent().remove();
})

function subir_archivo() {
	formData = new FormData(document.getElementById("form_file"));
		formData.append("accion", "subir");
	$.ajax({
		url: '../backend/core/src/modules/file_manage_ajax.php',
		type: 'POST',
		dataType: 'json',
		data: formData,
		contentType: false, //importante enviar este parametro en false
		processData: false,
		success:function(data){
			if(data.estado == 'ok'){
				//location.reload();
				$("#archivoSeleccionado").text('')
				toastr.success('Subido con exito');
			}else{
				$.alert(data.estado);
			}
		}
	})	
}


/* subir archivo de acerca*/
$(document).on("click", "#open_search", function(){
	$("#file-input-acerca").click();
})

$(document).on("change", "#file-input-acerca", function(){
	//console.log(this.files);
	var imagen = this.files;
	var elemento;
	var soportada = ["image/jpeg", "image/png", "image/gif"];
	var novalido = false;
	for (var i = 0; i < imagen.length; i++) {
		elemento = imagen[i];
		if (soportada.indexOf(elemento.type) != -1) {
			createPreviewAbout(elemento);
		}
		else {
			novalido = true;
		}
	}
})

function createPreviewAbout(dato) {
	var imgCodified = URL.createObjectURL(dato);
	$(".box-previa-about").css('display', 'none');
	document.getElementById("box-img-acerca").innerHTML='<section id="imagen" class="images-card"><div class="container-imagen"><figure><img src="'+imgCodified+'" alt=""><figcaption> <i class="fa fa-close"></i></figcaption></figure></div></section>';
}
$(document).on("click", "#imagen .container-imagen", function(e){
	document.getElementById("box-img-acerca").innerHTML='<div class="box-previa-about"></div>';
	$(this).parent().remove();
})

function guardar_imagen_acerca(){
	formData = new FormData(document.getElementById("form_img_acerca"));
	formData.append("accion", "subir_imagen");
	$.ajax({
		url: '../backend/core/src/modules/file_manage_ajax.php',
		type: 'POST',
		dataType: 'json',
		data: formData,
		contentType: false, //importante enviar este parametro en false
		processData: false,
		success:function(data){
			if(data.estado == 'ok'){
				toastr.success('Subido con exito');
			}else{
				$.alert(data.estado);
			}
		}
	})	
}
function actualizarAcerca(){
	dataString = $("#form_acerca").serialize();
	$.ajax({
		url: '../backend/core/src/controller/tbl_consultas.php',
		type: 'POST',
		dataType: 'json',
		data: dataString+"&accion=actualizarAcerca",
		success: function(data, statusCode, xhr){
			if(data.estado == "actualizado"){
				toastr["success"]("Actualizado")
			}else{
				toastr["error"]("A ocurrido un erro")
			}
		}
	})
}
// subir foto persona y personal administrativo
$(document).on("click", "#imagen-persona", function(){
	$("#file-input-persona").click();
})

function guardarPersona(){
	dataString = $("#form-personal").serialize();
	$.ajax({
		url: '../backend/core/src/controller/tbl_consultas.php',
		type: 'POST',
		dataType: 'json',
		data: dataString+"&accion=insertarPersona",
		success: function(data, statusCode, xhr){
			if(data.estado == "insertado"){
				toastr["success"]("Agregado con exito")
				updateImagenPersona(data.valor);
				$("#form-personal")[0].reset();
			}else{
				toastr["error"]("A ocurrido un erro")
			}
		}
	})
}
function updateImagenPersona(id){
	formData = new FormData(document.getElementById("form_img_persona"));
	formData.append("id", id);
	formData.append("accion", "subir_imagenPersona");
	$.ajax({
		url: '../backend/core/src/modules/file_manage_ajax.php',
		type: 'POST',
		dataType: 'json',
		data: formData,
		contentType: false, //importante enviar este parametro en false
		processData: false,
		success:function(data){
			if(data.estado == 'ok'){
				document.getElementById("box-img-personal").innerHTML='<div class="box-default"><i class="fa fa-camera" id="imagen-persona"></i></div>';

			}else{
				$.alert(data.estado);
			}
		}
	})	
}

function guardarPersonaAdm(){
	dataString = $("#form-personal").serialize();
	$.ajax({
		url: '../backend/core/src/controller/tbl_consultas.php',
		type: 'POST',
		dataType: 'json',
		data: dataString+"&accion=insertarPersonaAdm",
		success: function(data, statusCode, xhr){
			if(data.estado == "insertado"){
				toastr["success"]("Agregado con exito")
				updateImagenPersona(data.valor);
				$("#form-personal")[0].reset();
			}else{
				toastr["error"]("A ocurrido un erro")
			}
		}
	})
}


$(document).on("change", "#file-input-persona", function(){
	//console.log(this.files);
	var imagen = this.files;
	var elemento;
	var soportada = ["image/jpeg", "image/png", "image/gif"];
	var novalido = false;
	for (var i = 0; i < imagen.length; i++) {
		elemento = imagen[i];
		if (soportada.indexOf(elemento.type) != -1) {
			createPreviewPersona(elemento);
		}
		else {
			novalido = true;
		}
	}
})

function createPreviewPersona(dato) {
	var imgCodified = URL.createObjectURL(dato);
	$(".box-default").css('display', 'none');
	document.getElementById("box-img-personal").innerHTML='<div class="box-previa-persona"><img src="'+imgCodified+'" class=""><div class="images-card"><i class="fa fa-close"id="img-persona-close"></i></div></div>';
}
$(document).on("click", "#img-persona-close", function(e){
	document.getElementById("box-img-personal").innerHTML='<div class="box-default"><i class="fa fa-camera" id="imagen-persona"></i></div>';
	$(this).parent().remove();
})

// personal administrativo


/*estados, ciudad, municipio*/
$("select[name=id_estado]").change(function(){
	$(".ciudad select").empty();
	dato =($('select[name=id_estado]').val());
	$.ajax({
		type: "POST",
		url: '../../backend/core/src/controller/tbl_consultas.php',
		dataType: "json",
		data: "id_estado="+dato+"&accion=ciudad",
		success: function(data){
			$.each(data,function(index_data, registro) {
				$("#ciudad").append('<option value='+registro.id_ciudad+'>'+registro.ciudad+'</option>');
			})    
		},
		error: function(data) {
			alert('error');
		}
	})
})

$("select[name=id_ciudad]").change(function(){
	dato =($('select[name=id_ciudad]').val());
	$.ajax({
		type: "POST",
		url: '../../backend/core/src/controller/tbl_consultas.php',
		dataType: "json",
		data: "id_ciudad="+dato+"&accion=municipio",
		success: function(data){
			$.each(data,function(index_data, registro) {
				$("#municipio").val(registro.municipio);
				$("#id_municipio").val(registro.id_municipio);
			})    
		},
		error: function(data) {
			alert('error');
		}
	})
})

/*actualizar usuario*/
function actualizarUser(){
	dataString = $("#form_update_user").serialize();
	$.ajax({
		url: '../../backend/core/src/controller/tbl_usuario.php',
		type: 'POST',
		dataType: 'json',
		data: dataString+"&accion=actualizarUser",
		success: function(data, statusCode, xhr){
			if(data.estado == "actualizado"){
				toastr["success"]("Actualizado")
			}else{
				toastr["error"]("A ocurrido un erro")
			}
		}
	})
}

