<?php 
include 'modules/head.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/backend/core/src/model/Class_consultas.php';
require_once $_SERVER["DOCUMENT_ROOT"].'/backend/core/vendor/funciones.php';
$modelo = new Class_consultas();
$db = db;
$banner = $modelo->consultBanner($db);
$acerca = $modelo->consultAcerca($db);
$personal = $modelo->insertPersona($db);

// estados
$estado = $modelo->estado($db);
// puesto
$puesto = $modelo->puesto($db);

?>
<title><?php echo $nombresistema?>-Banner</title>
<div class="se-pre-con"></div>
	<section>
		<!-- sidebar menu start -->
	<?php include 'modules/sidebar.php' ?>
		<!-- //sidebar menu end -->
		<!-- header-starts -->
	<?php  include 'modules/header.php'?>
		<!-- //header-ends -->
		<!-- main content start -->
		<div class="main-content">
			<div class="container-fluid content-top-gap">
	<!-- //content -->
		<?php include 'panel/'.$_GET['v'].'.php'?>
		<!-- end main content -->
			</div>
		</div>
	</section>
<?php 
include 'modules/footer.php';
?>

