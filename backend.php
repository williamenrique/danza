<!DOCTYPE html>
<html lang="en">
<head>
<title>Login Admin</title>
 <!-- Meta-Tags -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="utf-8">
    <meta name="keywords" content="Business Login Form a Responsive Web Template, Bootstrap Web Templates, Flat Web Templates, Android Compatible Web Template, Smartphone Compatible Web Template, Free Webdesigns for Nokia, Samsung, LG, Sony Ericsson, Motorola Web Design">
    <script>
        addEventListener("load", function () {
            setTimeout(hideURLbar, 0);
        }, false);

        function hideURLbar() {
            window.scrollTo(0, 1);
        }
    </script>
    <!-- //Meta-Tags -->
			<style>
			.bd-placeholder-img {
				font-size: 1.125rem;
				text-anchor: middle;
				-webkit-user-select: none;
				-moz-user-select: none;
				-ms-user-select: none;
				user-select: none;
			}
			
			@media (min-width: 768px) {
				.bd-placeholder-img-lg {
					font-size: 3.5rem;
				}
			}
			</style>
	<!-- css files -->
	<link href="backend/core/vendor/css/font-awesome.min.css" rel="stylesheet" type="text/css" media="all">
	<link rel="stylesheet" href="backend/core/vendor/css/style-starter.css">
	<link rel="stylesheet" href="backend/core/vendor/css/toastr.min.css">
	<link rel="stylesheet" href="backend/core/vendor/css/jquery-confirm.css">
	<link rel="stylesheet" href="backend/core/vendor/css/styles-login.css">
	<link rel="stylesheet" href="backend/core/vendor/css/floating-labels.css">
	<!-- //css files -->
	<!-- google fonts -->
	<link href="//fonts.googleapis.com/css?family=Raleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
	<!-- //google fonts -->
</head>
<body>
	<?php include 'backend/forms/login.php';?>
	<script src="backend/core/vendor/js/jquery-3.3.1.min.js"></script>
	<script src="backend/core/vendor/js/jquery-1.10.2.min.js"></script>
	<script src="backend/core/vendor/js/jquery.nicescroll.js"></script>
	<script src="backend/core/vendor/js/scripts.js"></script>
	<script src="backend/core/vendor/js/bootstrap.min.js"></script>
	<script src="backend/core/vendor/js/toastr.min.js"></script>
	<script src="backend/core/vendor/js/jquery-confirm.js"></script>
	<script src="backend/assets/js/login.js"></script>
</body>
</html>